# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import http
from odoo.http import request


class WorkSignup(http.Controller):

    @http.route('/web/user/signup', type='json', auth='public')
    def rpc_signup(self, name, email, password):
        values = {'login': email, 'password': password, 'name': name}
        db, login, password = request.env['res.users'].sudo().signup(values, "")
        request.env.cr.commit()     # as authenticate will use its own cursor we need to commit the current transaction
        request.session.authenticate(db, login, password)
        return request.env['ir.http'].session_info()
