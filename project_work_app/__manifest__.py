# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Project Work Mobile",

    'summary': """
        Project work mobile app customization""",

    'description': """
        Project work mobile application customization
    """,

    'category': 'project',
    'version': '1.0',

    'depends': ['project'],
    'data': [
        'views/project_project.xml',
        'views/project_teams.xml',
        'security/record_rules.xml',
        'security/ir.model.access.csv'
    ],
    'application': True
}
