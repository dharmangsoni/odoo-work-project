# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class TeamMemberInvitations(models.Model):
    _name = "project.team.member.invitations"

    team_id = fields.Many2one("project.teams")
    member_id = fields.Many2one("res.partner")
    status = fields.Selection([('accepted', 'Accepted'), ('rejected', 'Rejected')])
