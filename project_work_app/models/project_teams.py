# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class ProjectTeams(models.Model):
    _name = "project.teams"
    _inherit = ['mail.thread']

    name = fields.Char(required=True)
    team_member_ids = fields.Many2many("res.partner", 'project_team_members', 'team_id', 'partner_id')
    invitation_member_ids = fields.Many2many("res.partner", 'project_team_invitation_members', 'team_id', 'partner_id')

    def leave_team(self, team_id, partner_id):
        user = self.env['res.users'].sudo().search([('partner_id', '=', partner_id)], limit=1)
        team = self.sudo().search([('id', '=', team_id)], limit=1)
        team.sudo().message_unsubscribe_users([user.id])
        team.sudo().write({
            'team_member_ids': [(6, 0, list(set(team.team_member_ids.ids) - set([partner_id])))],
            'invitation_member_ids': [(6, 0, list(set(team.invitation_member_ids.ids) - set([partner_id])))]
        })
        return True

    def action_invite_member(self):
        ctx = dict()
        ctx.update({
            'default_model': 'project.teams',
            'default_res_id': self.ids[0],
            'default_team_id': self.ids[0]
        })
        return {
            'name': 'Invite members',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'project.team.invitation',
            'target': 'new',
            'context': ctx,
            'active_id': self.ids[0]
        }

class invite_member(models.TransientModel):
    _name = 'project.team.invitation'

    team_id = fields.Many2one('project.teams')
    member_ids = fields.Many2many("res.partner", 'project_team_invitations', 'team_id', 'partner_id', string="Members")
    invited = fields.Boolean(default=False)

    @api.model
    def create(self, values):
        res = super(invite_member, self).create(values)
        if not res.invited:
            invited_ids = res.team_id.invitation_member_ids.ids
            accepted = res.team_id.team_member_ids.ids
            new_ids = res.member_ids.ids
            pending_invitation = list(set(new_ids) - set(invited_ids) - set(accepted))
            for member in res.member_ids:
                if member.id in pending_invitation:
                    print "SENDING INVIATIONS :", member.name
                    print "TEAM NAME : ", res.team_id.name
                    res.team_id.message_post(subject="Invitation", body="Invitation sent to %s" % (member.name), message_type="comment", subtype="mail.mt_comment", partner_ids=[(4, member.id)])
            all_invitations = list(pending_invitation + invited_ids)
            res.team_id.sudo().write({'invitation_member_ids': [(6, 0, all_invitations)]})
        return res


class MailChannel(models.Model):
    _inherit = 'mail.channel'

    @api.model
    def _fcm_prepare_payload(self, message):
        payload = super(MailChannel, self)._fcm_prepare_payload(message)
        if message.model == "project.teams" and message.subject == "Invitation":
            payload['type'] = "invitation"
        return payload
